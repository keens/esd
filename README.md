# Build
mvn clean package && docker build -t de.dpunkt/my-aktion .

# RUN
docker rm -f my-aktion || true && docker run -d -p 8080:8080 -p 4848:4848 --name my-aktion de.dpunkt/my-aktion 

# Ziele
Das Ziel des Projektes ist funktionale Test für die Anwendung my-aktion zu erstellen. Dabei soll das Framework Arquillian mit der Erweiterung Graphene verwendet werden, welches bereits im Buch behandelt wird.
Außerdem soll mit Ende des Projektes ein neuer Testfall für das funktionale Testen definiert werden. 
Zusammengefasst können 3 Hauptziele definiert werden:
1. Testfälle aus dem Buch mit einer portablen Version von Firefox 27 zum laufen zu bekommen
2. Die Dependencies aktualisieren, sodass die Testfälle unter neueren Browser-Versionen funktionieren

# Bezug zu MyAktion
Die bestehende Anwendung aus dem Buch - bis Kapitel 9.4 - soll mit Hilfe des Frameworks Arquillian und dessen Erweiterung Graphene getestet werden.
Die Erstellung von 3 Testfällen mit diesem Framework wird bereits im Buch in den Kapiteln 5, 7.7 und 8.2 behandelt. 
Diese Kapitel werden durchgearbeitet und anschließend die notwendingen Anpassungen vorgenommen.

# Maßnahmen zur Zielerreichen
> Die Testfälle des Buches mit der älteren Firefox zum Laufen bringen
> Wissen über aktuelle Dependencies aneignen
> Migration auf die neuere Version
> Die Testfälle mit einer neueren Version von Firefox starten

# Umsetzungsschritte
Bestehende Testfälle
1. der erste Schritt ist das Durchführen der bereits beschrieben Testfälle im Buch und die Installation der Version 27 des Firefox-Browsers.
2. Nachdem dies durchgeführt wurde, wurde die Anwendung neu deployed und die Tests mit mvn verify gestartet
3. Dabei kam das Problem auf, dass die Pfade nicht richtig im Browser eingegeben wurden und dadurch die Test-Dateien nicht gefunden wurden. Aus diesem Grund wurde in jeder Testpage der Pfad wie folgt angepasst:@Location("test/organizer/listCampaigns.jsf"). Es wurde jeweils test/ ergänzt.

Neue Dependencies
4. Anschließend wurden alle dependencies auf Ihre Aktualität überprüft. Diese Erkenntnisse sind im Punkt # Dependencies mit entsprechenenden Links beschrieben. Die neuesten Versionen können im https://mvnrepository.com/ nachgelesen werden.
5. Nachdem die aktuellste Dependency herausgefunden und auf Kompatibitlität überprüft wurde, wurden diese in der pom.xml ergänzt. Erneuert wurden im Abschnitt <dependencies> folgende dependencies:
    > jakarta.jakartaee-api: 8.0.0
    > selenium-java: 4.0.0-alpha-6
    > primefaces: 8.0
    > microprofile: 3.0
    > junit: 4.13
    > graphene-webdriver: 2.3.2
    > wildfly-arquillian-container-remote: 2.2.0.Final
In dem Abschnitt <dependencyManagement> wurden folgende dependencies hinzugefügt:
    > arquillian-bom: 1.7.0.Alpha2
    > selenium-bom: 3.13.0
    > arquillian-drone-bom: 2.0.1.Final
Und im Abschnitt <build> wurde folgendes ergänzt und erneuert
    > wildfly-maven-plugin: 2.0.2.Final
    > maven-surefire-plugin: 2.22.2
    > maven-failsafe-plugin: 2.22.2
6. Nach der Anpassung der Dependencies wurde die Anwendung erneut mit mvn verify deployed. Obwohl die dependencies neu definiert wurden, treten Probleme bei der Durchführung der Testfälle in der neuesten Version des Firefox-Browsers auf.
7. Im nächsten Schritt wurde überprüft, mit welchem Firefox-Browser die neuen Dependencies kompatibel sind. Die höchste Version des Firefox Browsers, welche mit Arquillian Graphene kompatibel ist, ist Firefox 44. Dies liegt daran, dass der Selenium FirefoxDriver keine neueren Versionen unterstützt.
8. Eine Lösung für dieses Problem ist den "Gecko Marionette Firefox Driver with Selenium 3.0" zu verwenden. Unter Folgendem Link ist die Installationsdatei zu finden: https://github.com/mozilla/geckodriver/releases. Für die Installation wurde diesen Guides gefolgt: https://www.swtestacademy.com/gecko-marionette-firefox-selenium/, https://www.toolsqa.com/selenium-webdriver/how-to-use-geckodriver/. Die Hauptänderung ist, dass in den Umgebungsvariablen die geckodriver.exe Datei angegegben werden muss.
9. Als Umgebungsvariable "webdriver.gecko.driver" wird der Pfad C:\Marionette\geckodriver.exe hinterlegt. Im PATH wird ebenfalls der Ordner angegeben: C:\Marionette. Dies wurde auch in der arquillian.xml hinterlegt: <property name="firefoxDriverBinary">C:\Marionette\geckodriver.exe</property>
10. Damit die Testfälle unter der neueren Browser-Version laufen, muss die zuvor durchgeführte Anpassung der Pfade wieder rückgängig gemacht werden. Das bedeutet in allen Pages "test/" löschen
11. mit mvn verify können nun die Testfälle gestartet werden
=> Nach der Website: https://firefox-source-docs.mozilla.org/testing/geckodriver/Support.html sind mit der aktuellen Version die Browser Firefox 55 - 62 unterstützt. 

# Ergebnis
Die Testfälle des Buches laufen nun unter der neueren Firefox Browser-Version: 62


# Dependencies

> Der folgende offizielle Guide des Arquillian Frameworks enthält geupdatete Dependecies der pom.xml: http://arquillian.org/guides/getting_started/

> Für das Verständnis der Erweiterung Graphene, ist eine gute Erklärung unter folgendem Link zu finden: http://arquillian.org/arquillian-graphene/

> die neueste Version von Graphene: http://arquillian.org/modules/graphene-extension/

> die neuste Arquillian Version: http://arquillian.org/modules/core-platform/

> die neuste Junit Version: https://junit.org/junit5/docs/current/user-guide/#dependency-metadata
--> Für die Migration auf Junit5 kann folgender Guide verwendet werden: https://www.baeldung.com/junit-5-migration
! Eine Migration nach Junit5 ist nach aktuellem Stand nicht ohne weiteres Möglich, da Arquillian dies nicht unterstützt. ! 