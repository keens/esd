package services;

import java.util.List;

import model.Campaign;

public interface CampaignService {
    
    List<Campaign> getAllCampaigns();

    Campaign addCampaign(Campaign campaign);

    void deleteCampaign(Campaign campaign);

    Campaign updateCampaign(Campaign campaign);

    void deleteCampaign(Long campaignId);

    Campaign getCampaign(Long campaignId);
}