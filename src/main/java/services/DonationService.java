package services;

import java.util.List;

import model.Donation;
import services.exceptions.ObjectNotFoundException;

public interface DonationService {
    List<Donation> getDonationList(Long campaignId);

    void addDonation(Long campaignId, Donation donation);

    void transferDonations();

    List<Donation> getDonationListPublic(Long campaignId) throws ObjectNotFoundException;
}